import React, { Component } from 'react';

class Patients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country:""
    };


  }
  
  handleCountryChange(ev){
    let e = document.getElementById('country');
    let country = e.options[e.selectedIndex].value;
    this.setState({
      country:country
    })
    ev.preventDefault();
  }

  deletePatient(id, ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    const { patientService } = this.props;

    /////////////////////////////////////////////////
    // WRITE THE CODE HERE TO REMOVE THE SELECTED CAR
    // START v
    /////////////////////////////////////////////////

    /////////////////////////////////////////////////
    // END ^
    /////////////////////////////////////////////////
  }
  
  handleSubmit(ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    const { patientService } = this.props;
    
    ///////////////////////////////////////
    // WRITE THE CODE HERE TO ADD A NEW CAR
    // START v
    ///////////////////////////////////////



    ///////////////////////////////////////
    // END ^
    ///////////////////////////////////////
    
    ev.preventDefault();
  }

  render() {
    const { patients } = this.props;
    let prov;
    let zip;
    if(this.state.country === 'usa'){
      prov = <div className="col-lg-2 col-md-4 mb-3">
                <label htmlFor="prov">State</label>
                <select className="form-control" id="prov" form="patientform">
                  <option value="wa">Washington</option>
                  <option value="or">Oregon</option>
                  <option value="ca">California</option>
                </select>
                <div className="invalid-feedback">
                    A country is required.
                </div>
              </div>;
      
      
      zip = <div className="col-lg-2 col-md-4 mb-3">
                <label htmlFor="zip">Zip Code</label>
                <input type="text" className="form-control" id="zip" defaultValue="" required />
                <div className="invalid-feedback">
                    A Zip Code is required.
                </div>
              </div>
    }
    else{
      prov = <div className="col-lg-2 col-md-4 mb-3">
                <label htmlFor="prov">Province</label>
                <select className="form-control" id="prov" form="patientform">
                  <option value="bc">BC</option>
                  <option value="ab">Alberta</option>
                  <option value="yk">Yukon</option>
                </select>
                <div className="invalid-feedback">
                    A country is required.
                </div>
              </div>;
              
              
              
      zip = <div className="col-lg-2 col-md-4 mb-3">
                <label htmlFor="zip">Postal Code</label>
                <input type="text" className="form-control" id="zip" defaultValue="" required />
                <div className="invalid-feedback">
                    A Postal Code is required.
                </div>
              </div>
    }

    return(
    <div>
      <div className="py-5 text-center">
        <h2>Patients</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.handleSubmit} className={this.state.formclass} noValidate id="patientform">
            <div className="row">
              <div className="col-md-4 mb-3">
                <label htmlFor="fName">First Name</label>
                <input type="text" className="form-control" id="fName" defaultValue="" required />
                <div className="invalid-feedback">
                    A first name is required.
                </div>
              </div>
              
              <div className="col-md-4 mb-3">
                <label htmlFor="lName">Last Name</label>
                <input type="text" className="form-control" id="lname" defaultValue="" required />
                <div className="invalid-feedback">
                    A last name is required.
                </div>
              </div>
              
              <div className="col-md-4 mb-3">
                <label htmlFor="email">E-Mail</label>
                <input type="text" className="form-control" id="email" defaultValue="" required />
                <div className="invalid-feedback">
                    An email is required.
                </div>
              </div>
              
              <div className="col-lg-2 col-md-4 mb-3">
                <label htmlFor="country">Country</label>
                <select onChange={this.handleCountryChange.bind(this)} className="form-control" id="country" form="patientform">
                  <option value="canada">Canada</option>
                  <option value="usa">USA</option>
                </select>
                <div className="invalid-feedback">
                    A country is required.
                </div>
              </div>
              
              
              {prov}
              
              {zip}
              
              <div className="col-lg-3 col-md-6 mb-3">
                <label htmlFor="lPres">Lowest Systolic Pressure</label>
                <input type="text" className="form-control" id="lPres" defaultValue="" required />
                <div className="invalid-feedback">
                    A lowest pressure is required.
                </div>
              </div>
              
              <div className="col-lg-3 col-md-6 mb-3">
                <label htmlFor="hPres">Highest Systolic Pressure</label>
                <input type="text" className="form-control" id="hPres" defaultValue="" required />
                <div className="invalid-feedback">
                    A highest pressure is required.
                </div>
              </div>
              
            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add patient</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Patients;
